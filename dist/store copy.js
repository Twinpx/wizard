//store/index.js
window.wizardStore = {
  activeStepIndex: 1,
  resultURL: 'result.json',
};

//store/modules/steps.js
window.wizardStoreSteps = [
  //id of the steps
  '6006',
  '6009',
  '6007',
  '6008',
  '6020',
  '6010',
];

//store/modules/stepMain.js
window.wizardStoreStep_6006 = {
  id: '6006',
  type: 'main',
  title: '',
  resultButton: false,
  images: [
    'https://images.unsplash.com/photo-1591431257693-d7c738049d71?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1824&q=80',
    'https://images.unsplash.com/photo-1616855200855-27b907993bfa?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2850&q=80',
    'https://images.unsplash.com/photo-1612460394261-531774bf9910?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1868&q=80',
  ],
};

//store/modules/stepLinks.js
window.wizardStoreStep_6007 = {
  id: '6007',
  type: 'links',
  title: 'Что бы вы хотели упаковать?',
  resultButton: false,
  form: {
    action: 'response.json',
    method: 'POST',
  },
  tabs: [
    {
      title: 'Потребительские товары',
      cards: [
        {
          id: '798',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из камня, глины и стекла',
        },
        {
          id: '799',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '800',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '801',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '802',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '803',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '804',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '805',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '806',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '807',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Промышленные товары',
      cards: [
        {
          id: '808',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из камня, глины и стекла',
        },
        {
          id: '809',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '810',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '811',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '812',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '813',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '814',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '815',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '816',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '817',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Пищевые товары',
      cards: [
        {
          id: '818',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из камня, глины и стекла',
        },
        {
          id: '819',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '820',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '821',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '822',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '823',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '824',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '825',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '826',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '827',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Другие товары',
      cards: [
        {
          id: '828',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из камня, глины и стекла',
        },
        {
          id: '829',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '830',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '831',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '832',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '833',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '834',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '835',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '836',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '837',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
      ],
    },
  ],
};

//store/modules/stepForm.js
window.wizardStoreStep_6008 = {
  id: '6008',
  type: 'form',
  title: 'Какие будут параметры упаковки?',
  resultButton: false,
  fields: {
    blocks: [
      {
        blockTitle: 'Внешние размеры',
        blockFields: [
          {
            type: 'input-range',
            label: 'Длина',
            name: 'ext-length',
            min: 0,
            max: 800,
            step: 1,
            value: 380,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Ширина',
            name: 'ext-width',
            min: 0,
            max: 600,
            step: 1,
            value: 450,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Высота',
            name: 'ext-height',
            min: 0,
            max: 400,
            step: 1,
            value: 100,
            unit: 'мм',
          },
        ],
      },
      {
        blockTitle: 'Внутренние размеры',
        blockFields: [
          {
            type: 'input-range',
            label: 'Длина',
            name: 'int-length',
            min: 0,
            max: 1800,
            step: 1,
            value: 1380,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Ширина',
            name: 'int-width',
            min: 0,
            max: 1600,
            step: 1,
            value: 1450,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Высота',
            name: 'int-height',
            min: 0,
            max: 1400,
            step: 1,
            value: 1100,
            unit: 'мм',
          },
        ],
      },
      {
        blockTitle: 'Толщина',
        blockFields: [
          {
            type: 'input-radio-img',
            label: '1-но слойный гофрокартон',
            name: 'thickness',
            value: '1',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1520038410233-7141be7e6f97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=753&q=80',
            note:
              '<div class="row"><div class="col-sm-6"><img src="./popup-img.png" alt="" class="img-responsive"></div><hr class="visible-xs-block"><div class="col-sm-6"><h3 style="margin-left: 15px;">Тиснение фольгой</h3><ul><li>В разы увеличивает привлекательность упаковки</li><li>Имеет большую вариативность по цветам и эффектам</li><li>Является уникальной и бюджетной альтернативой цветной печати</li><li>Может применяться на большом формате упаковки</li><li>Хорошо сочетается с другими опциями, например, печатью, вырубными окошками и тд.</li><li>Создает приятные тактильные ощущения</li><li>Сочетается с любым видом и цветом гофрокартона</li></ul></div></div>',
          },
          {
            type: 'input-radio-img',
            label: '2-х слойный гофрокартон',
            name: 'thickness',
            value: '2',
            checked: true,
            img:
              'https://images.unsplash.com/photo-1556229040-2a7bc8a00a3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          },
          {
            type: 'input-radio-img',
            label: '3-х слойный гофрокартон',
            name: 'thickness',
            value: '3',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1598935888738-cd2622bcd437?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          },
        ],
      },
      {
        blockTitle: 'Цвет',
        blockFields: [
          {
            type: 'input-radio-img',
            label: 'Белый',
            name: 'color',
            value: 'white',
            checked: true,
            img:
              'https://images.unsplash.com/photo-1520038410233-7141be7e6f97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=753&q=80',
          },
          {
            type: 'input-radio-img',
            label: 'Бурый',
            name: 'color',
            value: 'brown',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1556229040-2a7bc8a00a3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          },
        ],
      },
      {
        blockTitle: 'Тираж',
        blockFields: [
          {
            type: 'input-range',
            label: 'Количество',
            name: 'quantity',
            min: 0,
            max: 1000,
            step: 1,
            value: 50,
            unit: 'шт',
          },
        ],
      },
    ],
  },
};

//store/modules/stepMultiselect.js
window.wizardStoreStep_6009 = {
  id: '6009',
  type: 'multiselect',
  title: 'Нужны ли дополнительные опции?',
  resultButton: false,
  tabs: [
    {
      title: 'Потребительские товары',
      cards: [
        {
          id: '798',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из камня, глины и стекла',
        },
        {
          id: '799',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '800',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '801',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '802',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '803',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '804',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '805',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '806',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '807',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Промышленные товары',
      cards: [
        {
          id: '808',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из камня, глины и стекла',
        },
        {
          id: '809',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '810',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '811',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '812',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '813',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '814',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '815',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '816',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '817',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Пищевые товары',
      cards: [
        {
          id: '818',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из камня, глины и стекла',
        },
        {
          id: '819',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '820',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '821',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '822',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '823',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '824',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '825',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '826',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '827',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Другие товары',
      cards: [
        {
          id: '828',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из камня, глины и стекла',
        },
        {
          id: '829',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '830',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '831',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '832',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '833',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '834',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '835',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '836',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '837',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
      ],
    },
  ],
};

//store/modules/stepPoll.js
window.wizardStoreStep_6010 = {
  id: '6010',
  type: 'poll',
  title: 'На чем важно сделать акцент?',
  resultButton: true,
  tabs: [
    {
      title: 'Потребительские товары',
      checkbox: [
        {
          name: 'speed1',
          checked: true,
          label: 'Скорость изготовления',
        },
        {
          name: 'wow1',
          checked: false,
          label: 'Wow эффект',
        },
        {
          name: 'simplicity1',
          checked: true,
          label: 'Простота утилизации',
        },
        {
          name: 'price1',
          checked: false,
          label: 'Цена',
        },
      ],
    },
  ],
};

//store/modules/stepLinks.js
window.wizardStoreStep_6020 = {
  id: '6020',
  type: 'links',
  title: 'Дополнительный шаг с ссылками',
  resultButton: false,
  form: {
    action: 'response.json',
    method: 'POST',
  },
  tabs: [
    {
      title: 'Товары для потребления',
      cards: [
        {
          id: '798',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из камня, глины и стекла',
        },
        {
          id: '799',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '800',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '801',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '802',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '803',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '804',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '805',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '806',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '807',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Товары для промышленности',
      cards: [
        {
          id: '808',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из камня, глины и стекла',
        },
        {
          id: '809',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '810',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '811',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '812',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '813',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '814',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '815',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '816',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '817',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Пищевые товары',
      cards: [
        {
          id: '818',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из камня, глины и стекла',
        },
        {
          id: '819',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '820',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '821',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '822',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '823',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '824',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '825',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '826',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '827',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Другие товары',
      cards: [
        {
          id: '828',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из камня, глины и стекла',
        },
        {
          id: '829',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '830',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '831',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '832',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '833',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '834',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '835',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '836',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '837',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
      ],
    },
  ],
};
