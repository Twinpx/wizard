Number.prototype.format = function() {
  return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
};
String.prototype.deformat = function() {
  return Number(
    this.toString()
      .replace(/\D/g, '')
      .split(' ')
      .join('')
  );
};

import { createApp } from 'vue';
import App from './App.vue';
import store from './store';
import Maska from '../node_modules/maska/dist/maska';

document.querySelectorAll('.wizard-app').forEach((wizardApp) => {
  const id = `wizard-${parseInt(new Date().getTime() * Math.random())}`;
  wizardApp.setAttribute('id', id);

  window.formOpen[id] = false;

  const app = createApp(App);
  app.use(store);
  app.use(Maska);
  app.mount(`#${id}`);
});
