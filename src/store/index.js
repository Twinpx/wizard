import { createStore } from 'vuex';
import stepsData from './modules/stepsData';
import steps from './modules/steps';

export default createStore({
  state: () => ({
    ...window.wizardStore,
    formOpen: window.formOpen,
    textareaValue: '',
    activeStepIndex: 0,
  }),
  modules: {
    stepsData,
    steps,
  },
  mutations: {
    showHideForm(state, payload) {
      //yandex metrika
      if (payload.flag) {
        if (window[`yaCounter${window.ymId || 103630}`]) {
          //yaCounter
          window[`yaCounter${window.ymId || 103630}`].reachGoal(
            'WIZARD-OPEN-FORM'
          );
        } else if (window.ym) {
          //ym
          window.ym(window.ymId || 103630, 'reachGoal', 'WIZARD-OPEN-FORM');
        }
      }
      //open the form
      state.formOpen[payload.id] = payload.flag;

      //set textarea value
      let str = '';
      let val = '';
      let filtered = [];

      for (let i = 0; i < state.activeStepIndex; i++) {
        const stepId = state.steps[i];
        filtered.push(
          state.stepsData.filter((stepObject) => {
            return stepObject.id === stepId;
          })[0]
        );
      }

      filtered.forEach((stepObject) => {
        val = '';
        switch (stepObject.type) {
          case 'links':
            stepObject.tabs.forEach((tab) => {
              tab.cards.forEach((card) => {
                if (card.checked) {
                  val += `${card.title}, `;
                }
              });
            });
            break;

          case 'multiselect':
            stepObject.tabs.forEach((tab) => {
              tab.cards.forEach((card) => {
                if (card.checked) {
                  val += `${card.title}, `;
                }
              });
            });
            break;

          case 'form':
            stepObject.fields.blocks.forEach((block) => {
              block.blockFields.forEach((field, index, fieldsArray) => {
                if (field.type !== 'input-radio-img') {
                  val += `${field.label}: ${field.value}, `;
                } else if (field.type === 'input-radio-img' && index === 0) {
                  fieldsArray.forEach((radioField) => {
                    if (radioField.checked) {
                      val += `${block.blockTitle}: ${radioField.label}, `;
                    }
                  });
                }
              });
            });
            break;

          case 'poll':
            stepObject.tabs.forEach((tab) => {
              tab.checkbox.forEach((checkbox) => {
                if (JSON.parse(checkbox.checked)) {
                  val += `${checkbox.name}, `;
                }
              });
            });
            break;
        }
        if (val) {
          val = val.substring(0, val.length - 2) + '.';
          val = val.replace('<br>', '');
          str += `${stepObject.title}\n${val}\n\n`;
        }
      });
      state.textareaValue = str;
    },
    setActiveStepIndex(state, payload) {
      state.activeStepIndex = payload.step;
      //scroll to top
      const y =
        document.getElementById(payload.wizardId).getBoundingClientRect().top +
        window.scrollY;
      //header height
      let headerHeight = 0;
      if (document.querySelectorAll('.b-header').length) {
        headerHeight = document.querySelector('.b-header').clientHeight;
      }
      window.scrollTo({ top: y - headerHeight, behavior: 'smooth' });
    },
    pushResultStepLinks(state, payload) {
      const step = state.stepsData.filter(
        (stepObject) => stepObject.id === payload.stepId
      )[0];
      step.tabs[payload.activeTab].cards[payload.cardIndex].checked = true;
    },
    pushResultStepMultiselect(state, payload) {
      const step = state.stepsData.filter(
        (stepObject) => stepObject.id === payload.stepId
      )[0];
      step.tabs[payload.activeTab].cards[payload.cardIndex].checked =
        payload.checked;
    },
    pushResultStepForm(state, payload) {
      const step = state.stepsData.filter(
        (stepObject) => stepObject.id === payload.stepId
      )[0];
      const field =
        step.fields.blocks[payload.blockIndex].blockFields[payload.fieldIndex];

      if (payload.checked === true || payload.checked === false) {
        step.fields.blocks[payload.blockIndex].blockFields.forEach((field) => {
          field.checked = false;
        });
      }

      payload.checked === true || payload.checked === false
        ? (field.checked = payload.checked)
        : (field.value = payload.value);
    },
    pushResultStepPoll(state, payload) {
      const step = state.stepsData.filter(
        (stepObject) => stepObject.id === payload.stepId
      )[0];
      step.tabs[payload.activeTab].checkbox[payload.itemIndex].checked =
        payload.checked;
    },
  },
});
