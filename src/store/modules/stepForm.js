const state = () => ( window.wizardStoreStepForm /*{
  title: 'Какие будут параметры упаковки?',
  resultButton: false,
  fields: {
    blocks: [
      {
        blockTitle: 'Внешние размеры',
        blockFields: [
          {
            type: 'input-range',
            label: 'Длина',
            name: 'ext-length',
            min: 0,
            max: 800,
            step: 1,
            value: 380,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Ширина',
            name: 'ext-width',
            min: 0,
            max: 600,
            step: 1,
            value: 450,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Высота',
            name: 'ext-height',
            min: 0,
            max: 400,
            step: 1,
            value: 100,
            unit: 'мм',
          },
        ],
      },
      {
        blockTitle: 'Внутренние размеры',
        blockFields: [
          {
            type: 'input-range',
            label: 'Длина',
            name: 'int-length',
            min: 0,
            max: 1800,
            step: 1,
            value: 1380,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Ширина',
            name: 'int-width',
            min: 0,
            max: 1600,
            step: 1,
            value: 1450,
            unit: 'мм',
          },
          {
            type: 'input-range',
            label: 'Высота',
            name: 'int-height',
            min: 0,
            max: 1400,
            step: 1,
            value: 1100,
            unit: 'мм',
          },
        ],
      },
      {
        blockTitle: 'Толщина',
        blockFields: [
          {
            type: 'input-radio-img',
            label: '1-но слойный гофрокартон',
            name: 'thickness',
            value: '1',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1520038410233-7141be7e6f97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=753&q=80',
            note: '<h1>Тиснение фольгой</h1>',
          },
          {
            type: 'input-radio-img',
            label: '2-х слойный гофрокартон',
            name: 'thickness',
            value: '2',
            checked: true,
            img:
              'https://images.unsplash.com/photo-1556229040-2a7bc8a00a3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          },
          {
            type: 'input-radio-img',
            label: '3-х слойный гофрокартон',
            name: 'thickness',
            value: '3',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1598935888738-cd2622bcd437?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          },
        ],
      },
      {
        blockTitle: 'Цвет',
        blockFields: [
          {
            type: 'input-radio-img',
            label: 'Белый',
            name: 'color',
            value: 'white',
            checked: true,
            img:
              'https://images.unsplash.com/photo-1520038410233-7141be7e6f97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=753&q=80',
          },
          {
            type: 'input-radio-img',
            label: 'Бурый',
            name: 'color',
            value: 'brown',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1556229040-2a7bc8a00a3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          },
        ],
      },
      {
        blockTitle: 'Тираж',
        blockFields: [
          {
            type: 'input-range',
            label: 'Количество',
            name: 'quantity',
            min: 0,
            max: 1000,
            step: 1,
            value: 50,
            unit: 'шт',
          },
        ],
      },
    ],
  },
}*/);

export default {
  state,
};
