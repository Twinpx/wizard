const state = () => ( window.wizardStoreStepMultiselect /*{
  title: 'Нужны ли дополнительные опции?',
  resultButton: false,
  tabs: [
    {
      title: 'Потребительские товары',
      cards: [
        {
          id: '798',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из камня, глины и стекла',
        },
        {
          id: '799',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '800',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '801',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '802',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '803',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '804',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '805',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '806',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
        {
          id: '807',
          img:
            'https://images.unsplash.com/photo-1590422749897-47036da0b0ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          title: 'Изделия из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Промышленные товары',
      cards: [
        {
          id: '808',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из камня, глины и стекла',
        },
        {
          id: '809',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '810',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '811',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '812',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '813',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '814',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '815',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '816',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
        {
          id: '817',
          img:
            'https://images.unsplash.com/photo-1481401908818-600b7a676c0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=751&q=80 751w',
          title: 'Промышленные товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Пищевые товары',
      cards: [
        {
          id: '818',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из камня, глины и стекла',
        },
        {
          id: '819',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '820',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '821',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '822',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '823',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '824',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '825',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '826',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
        {
          id: '827',
          img:
            'https://images.unsplash.com/photo-1595446472901-b0988e150f9c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Пищевые товары из керамики, металла и пластика',
        },
      ],
    },
    {
      title: 'Другие товары',
      cards: [
        {
          id: '828',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из камня, глины и стекла',
        },
        {
          id: '829',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '830',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '831',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '832',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '833',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '834',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '835',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '836',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
        {
          id: '837',
          img:
            'https://images.unsplash.com/photo-1605117012605-b68dedd4accc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80 334w',
          title: 'Другие товары из керамики, металла и пластика',
        },
      ],
    },
  ],
}*/);

export default {
  state,
};
