let stepsData = window.wizardStoreSteps.map((step) => {
    return window[`wizardStoreStep_${step}`];
});

export default {
  state: stepsData
};
