//store/index.js
window.wizardStore = {
  resultURL: 'result.json',
  formAction: 'form.php',
  formMethod: 'POST',
};

//form open
window.formOpen = {};

//store/modules/steps.js
window.wizardStoreSteps = [
  //id of the steps
  '6009',
  '6007',
  '6011',
  '6008',
  '6017',
  '6010',
];

window.wizardStoreStep_60006 = {
  id: '60006',
  type: 'main',
  title: 'Подберем упаковку за',
  steps: true,
  resultButton: false,
  images: [
    'https://images.unsplash.com/photo-1591431257693-d7c738049d71?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1824&q=80',
  ],
};
window.wizardStoreStep_60012 = {
  id: '60012',
  type: 'main',
  title: 'Изготовим упаковку по вашему дизайну',
  steps: false,
  resultButton: false,
  images: [
    'https://images.unsplash.com/photo-1616855200855-27b907993bfa?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2850&q=80',
  ],
};
window.wizardStoreStep_60016 = {
  id: '60016',
  type: 'main',
  title: 'Новое предложение от компании Антек',
  steps: false,
  resultButton: false,
  images: [
    'https://images.unsplash.com/photo-1612460394261-531774bf9910?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1868&q=80',
  ],
};

window.wizardStoreStep_6007 = {
  id: '6007',
  type: 'links',
  title: 'Что бы вы хотели упаковать?',
  resultButton: false,
  tabs: [
    {
      title:
        '\u041f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u044b\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
      cards: [
        {
          id: 'avtomobilnaya-promyshlennost',
          img: '/upload/uf/d71/def.svg',
          title:
            '\u0410\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044c\u043d\u0430\u044f \u043f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0441\u0442\u044c',
        },
        {
          id: 'instrumenty',
          img: '/upload/uf/0ff/def.svg',
          title:
            '\u0418\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u044b',
        },
        {
          id: 'logistika-i-transportirovka',
          img: '/upload/uf/b04/def.svg',
          title:
            '\u041b\u043e\u0433\u0438\u0441\u0442\u0438\u043a\u0430\u003Cbr\u003E \u0438 \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430',
        },
        {
          id: 'sypuchie-gruzy',
          img: '/upload/uf/18a/def.svg',
          title:
            '\u0421\u044b\u043f\u0443\u0447\u0438\u0435 \u0433\u0440\u0443\u0437\u044b',
        },
        {
          id: 'farmatsevtika',
          img: '/upload/uf/773/def.svg',
          title:
            '\u0424\u0430\u0440\u043c\u0430\u0446\u0435\u0432\u0442\u0438\u043a\u0430',
        },
        {
          id: 'tsepi-i-kabeli',
          img: '/upload/uf/832/def.svg',
          title:
            '\u0426\u0435\u043f\u0438 \u0438 \u043a\u0430\u0431\u0435\u043b\u0438',
        },
        {
          id: 'zhidkosti',
          img: '/upload/uf/2da/def.svg',
          title: '\u0416\u0438\u0434\u043a\u043e\u0441\u0442\u0438',
        },
        {
          id: 'khimiya',
          img: '/upload/uf/f39/def.svg',
          title: '\u0425\u0438\u043c\u0438\u044f',
        },
        {
          id: 'elektrooborudovanie-i-elektronnye-pribory',
          img: '/upload/uf/31a/def.svg',
          title:
            '\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435 \u0438 \u044d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u044b\u0435 \u043f\u0440\u0438\u0431\u043e\u0440\u044b',
        },
        {
          id: 'promyshlennoe-oborudovanie-i-stanki',
          img: '/upload/uf/c14/def.svg',
          title:
            '\u041f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0435 \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435 \u0438 \u0441\u0442\u0430\u043d\u043a\u0438',
        },
        {
          id: 'zapchasti-agregaty-i-detali',
          img: '/upload/uf/234/def.svg',
          title:
            '\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u0438, \u0430\u0433\u0440\u0435\u0433\u0430\u0442\u044b \u0438 \u0434\u0435\u0442\u0430\u043b\u0438',
        },
      ],
    },
    {
      title:
        '\u041f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u044c\u0441\u043a\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
      cards: [
        {
          id: 'bytovaya-tekhnika',
          img: '/upload/uf/b5b/def.svg',
          title:
            '\u0411\u044b\u0442\u043e\u0432\u0430\u044f \u0442\u0435\u0445\u043d\u0438\u043a\u0430',
        },
        {
          id: 'detskie-tovary',
          img: '/upload/uf/7b5/babytoys.svg',
          title:
            '\u0414\u0435\u0442\u0441\u043a\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
        },
        {
          id: 'izdeliya-iz-kamnya-gliny-i-stekla',
          img: '/upload/uf/928/glass.svg',
          title:
            '\u0418\u0437\u0434\u0435\u043b\u0438\u044f \u0438\u0437 \u043a\u0430\u043c\u043d\u044f, \u0433\u043b\u0438\u043d\u044b \u0438 \u0441\u0442\u0435\u043a\u043b\u0430',
        },
        {
          id: 'mebel',
          img: '/upload/uf/91c/mebel.jpg',
          title: '\u041c\u0435\u0431\u0435\u043b\u044c',
        },
        {
          id: 'odezhda-obuv-i-aksessuary',
          img: '/upload/uf/de2/def.svg',
          title:
            '\u041e\u0434\u0435\u0436\u0434\u0430 \u043e\u0431\u0443\u0432\u044c \u0438 \u0430\u043a\u0441\u0435\u0441\u0441\u0443\u0430\u0440\u044b',
        },
        {
          id: 'okna-i-dveri',
          img: '/upload/uf/040/windows.svg',
          title:
            '\u041e\u043a\u043d\u0430 \u0438 \u0434\u0432\u0435\u0440\u0438',
        },
        {
          id: 'pechatnye-izdaniya',
          img: '/upload/uf/998/def.svg',
          title:
            '\u041f\u0435\u0447\u0430\u0442\u043d\u044b\u0435 \u0438\u0437\u0434\u0430\u043d\u0438\u044f',
        },
        {
          id: 'stroitelnye-i-otdelochnye-materialy',
          img: '/upload/uf/e93/def.svg',
          title:
            '\u0421\u0442\u0440\u043e\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u0438 \u043e\u0442\u0434\u0435\u043b\u043e\u0447\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b',
        },
        {
          id: 'tovary-dlya-doma',
          img: '/upload/uf/1dc/def.svg',
          title:
            '\u0422\u043e\u0432\u0430\u0440\u044b \u0434\u043b\u044f \u0434\u043e\u043c\u0430',
        },
        {
          id: 'tovary-dlya-zdorovya-i-krasoty',
          img: '/upload/uf/7b4/cosmetics.svg',
          title:
            '\u0422\u043e\u0432\u0430\u0440\u044b \u0434\u043b\u044f \u0437\u0434\u043e\u0440\u043e\u0432\u044c\u044f \u0438 \u043a\u0440\u0430\u0441\u043e\u0442\u044b',
        },
        {
          id: 'tsvety-i-rasteniya',
          img: '/upload/uf/1bd/def.svg',
          title:
            '\u0426\u0432\u0435\u0442\u044b \u0438 \u0440\u0430\u0441\u0442\u0435\u043d\u0438\u044f',
        },
        {
          id: 'elektronika-i-tekhnika',
          img: '/upload/uf/534/computers.svg',
          title:
            '\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u0438\u043a\u0430 \u0438 \u0442\u0435\u0445\u043d\u0438\u043a\u0430',
        },
      ],
    },
    {
      title:
        '\u041f\u0438\u0449\u0435\u0432\u0430\u044f \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u044f',
      cards: [
        {
          id: 'bakaleya',
          img: '/upload/uf/8a6/def.svg',
          title: '\u0411\u0430\u043a\u0430\u043b\u0435\u044f',
        },
        {
          id: 'konditerskie-izdeliya',
          img: '/upload/uf/4fa/def.svg',
          title:
            '\u041a\u043e\u043d\u0434\u0438\u0442\u0435\u0440\u0441\u043a\u0438\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f',
        },
        {
          id: 'myaso-i-ryba',
          img: '/upload/uf/c70/def.svg',
          title: '\u041c\u044f\u0441\u043e \u0438 \u0440\u044b\u0431\u0430',
        },
        {
          id: 'napitki',
          img: '/upload/uf/654/def.svg',
          title: '\u041d\u0430\u043f\u0438\u0442\u043a\u0438',
        },
        {
          id: 'ovoshchi-i-frukty',
          img: '/upload/uf/ce1/def.svg',
          title:
            '\u041e\u0432\u043e\u0449\u0438 \u0438 \u0444\u0440\u0443\u043a\u0442\u044b',
        },
        {
          id: 'pitstsa',
          img: '/upload/uf/6f7/def.svg',
          title: '\u041f\u0438\u0446\u0446\u0430',
        },
        {
          id: 'khlebobulochnye-izdeliya',
          img: '/upload/uf/f0f/def.svg',
          title:
            '\u0425\u043b\u0435\u0431\u043e\u0431\u0443\u043b\u043e\u0447\u043d\u044b\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f',
        },
      ],
    },
  ],
};

window.wizardStoreStep_6017 = {
  id: '6017',
  type: 'links',
  title: 'Что бы вы хотели упаковать?',
  resultButton: false,
  tabs: [
    {
      title:
        '\u041f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u044b\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
      cards: [
        {
          id: 'avtomobilnaya-promyshlennost',
          img: '/upload/uf/d71/def.svg',
          title:
            '\u0410\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044c\u043d\u0430\u044f \u043f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0441\u0442\u044c',
        },
        {
          id: 'instrumenty',
          img: '/upload/uf/0ff/def.svg',
          title:
            '\u0418\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u044b',
        },
        {
          id: 'logistika-i-transportirovka',
          img: '/upload/uf/b04/def.svg',
          title:
            '\u041b\u043e\u0433\u0438\u0441\u0442\u0438\u043a\u0430\u003Cbr\u003E \u0438 \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430',
        },
        {
          id: 'sypuchie-gruzy',
          img: '/upload/uf/18a/def.svg',
          title:
            '\u0421\u044b\u043f\u0443\u0447\u0438\u0435 \u0433\u0440\u0443\u0437\u044b',
        },
        {
          id: 'farmatsevtika',
          img: '/upload/uf/773/def.svg',
          title:
            '\u0424\u0430\u0440\u043c\u0430\u0446\u0435\u0432\u0442\u0438\u043a\u0430',
        },
        {
          id: 'tsepi-i-kabeli',
          img: '/upload/uf/832/def.svg',
          title:
            '\u0426\u0435\u043f\u0438 \u0438 \u043a\u0430\u0431\u0435\u043b\u0438',
        },
        {
          id: 'zhidkosti',
          img: '/upload/uf/2da/def.svg',
          title: '\u0416\u0438\u0434\u043a\u043e\u0441\u0442\u0438',
        },
        {
          id: 'khimiya',
          img: '/upload/uf/f39/def.svg',
          title: '\u0425\u0438\u043c\u0438\u044f',
        },
        {
          id: 'elektrooborudovanie-i-elektronnye-pribory',
          img: '/upload/uf/31a/def.svg',
          title:
            '\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435 \u0438 \u044d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u044b\u0435 \u043f\u0440\u0438\u0431\u043e\u0440\u044b',
        },
        {
          id: 'promyshlennoe-oborudovanie-i-stanki',
          img: '/upload/uf/c14/def.svg',
          title:
            '\u041f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0435 \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435 \u0438 \u0441\u0442\u0430\u043d\u043a\u0438',
        },
        {
          id: 'zapchasti-agregaty-i-detali',
          img: '/upload/uf/234/def.svg',
          title:
            '\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u0438, \u0430\u0433\u0440\u0435\u0433\u0430\u0442\u044b \u0438 \u0434\u0435\u0442\u0430\u043b\u0438',
        },
      ],
    },
    {
      title:
        '\u041f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u044c\u0441\u043a\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
      cards: [
        {
          id: 'bytovaya-tekhnika',
          img: '/upload/uf/b5b/def.svg',
          title:
            '\u0411\u044b\u0442\u043e\u0432\u0430\u044f \u0442\u0435\u0445\u043d\u0438\u043a\u0430',
        },
        {
          id: 'detskie-tovary',
          img: '/upload/uf/7b5/babytoys.svg',
          title:
            '\u0414\u0435\u0442\u0441\u043a\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
        },
        {
          id: 'izdeliya-iz-kamnya-gliny-i-stekla',
          img: '/upload/uf/928/glass.svg',
          title:
            '\u0418\u0437\u0434\u0435\u043b\u0438\u044f \u0438\u0437 \u043a\u0430\u043c\u043d\u044f, \u0433\u043b\u0438\u043d\u044b \u0438 \u0441\u0442\u0435\u043a\u043b\u0430',
        },
        {
          id: 'mebel',
          img: '/upload/uf/91c/mebel.jpg',
          title: '\u041c\u0435\u0431\u0435\u043b\u044c',
        },
        {
          id: 'odezhda-obuv-i-aksessuary',
          img: '/upload/uf/de2/def.svg',
          title:
            '\u041e\u0434\u0435\u0436\u0434\u0430 \u043e\u0431\u0443\u0432\u044c \u0438 \u0430\u043a\u0441\u0435\u0441\u0441\u0443\u0430\u0440\u044b',
        },
        {
          id: 'okna-i-dveri',
          img: '/upload/uf/040/windows.svg',
          title:
            '\u041e\u043a\u043d\u0430 \u0438 \u0434\u0432\u0435\u0440\u0438',
        },
        {
          id: 'pechatnye-izdaniya',
          img: '/upload/uf/998/def.svg',
          title:
            '\u041f\u0435\u0447\u0430\u0442\u043d\u044b\u0435 \u0438\u0437\u0434\u0430\u043d\u0438\u044f',
        },
        {
          id: 'stroitelnye-i-otdelochnye-materialy',
          img: '/upload/uf/e93/def.svg',
          title:
            '\u0421\u0442\u0440\u043e\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u0438 \u043e\u0442\u0434\u0435\u043b\u043e\u0447\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b',
        },
        {
          id: 'tovary-dlya-doma',
          img: '/upload/uf/1dc/def.svg',
          title:
            '\u0422\u043e\u0432\u0430\u0440\u044b \u0434\u043b\u044f \u0434\u043e\u043c\u0430',
        },
        {
          id: 'tovary-dlya-zdorovya-i-krasoty',
          img: '/upload/uf/7b4/cosmetics.svg',
          title:
            '\u0422\u043e\u0432\u0430\u0440\u044b \u0434\u043b\u044f \u0437\u0434\u043e\u0440\u043e\u0432\u044c\u044f \u0438 \u043a\u0440\u0430\u0441\u043e\u0442\u044b',
        },
        {
          id: 'tsvety-i-rasteniya',
          img: '/upload/uf/1bd/def.svg',
          title:
            '\u0426\u0432\u0435\u0442\u044b \u0438 \u0440\u0430\u0441\u0442\u0435\u043d\u0438\u044f',
        },
        {
          id: 'elektronika-i-tekhnika',
          img: '/upload/uf/534/computers.svg',
          title:
            '\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u0438\u043a\u0430 \u0438 \u0442\u0435\u0445\u043d\u0438\u043a\u0430',
        },
      ],
    },
    {
      title:
        '\u041f\u0438\u0449\u0435\u0432\u0430\u044f \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u044f',
      cards: [
        {
          id: 'bakaleya',
          img: '/upload/uf/8a6/def.svg',
          title: '\u0411\u0430\u043a\u0430\u043b\u0435\u044f',
        },
        {
          id: 'konditerskie-izdeliya',
          img: '/upload/uf/4fa/def.svg',
          title:
            '\u041a\u043e\u043d\u0434\u0438\u0442\u0435\u0440\u0441\u043a\u0438\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f',
        },
        {
          id: 'myaso-i-ryba',
          img: '/upload/uf/c70/def.svg',
          title: '\u041c\u044f\u0441\u043e \u0438 \u0440\u044b\u0431\u0430',
        },
        {
          id: 'napitki',
          img: '/upload/uf/654/def.svg',
          title: '\u041d\u0430\u043f\u0438\u0442\u043a\u0438',
        },
        {
          id: 'ovoshchi-i-frukty',
          img: '/upload/uf/ce1/def.svg',
          title:
            '\u041e\u0432\u043e\u0449\u0438 \u0438 \u0444\u0440\u0443\u043a\u0442\u044b',
        },
        {
          id: 'pitstsa',
          img: '/upload/uf/6f7/def.svg',
          title: '\u041f\u0438\u0446\u0446\u0430',
        },
        {
          id: 'khlebobulochnye-izdeliya',
          img: '/upload/uf/f0f/def.svg',
          title:
            '\u0425\u043b\u0435\u0431\u043e\u0431\u0443\u043b\u043e\u0447\u043d\u044b\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f',
        },
      ],
    },
  ],
};

window.wizardStoreStep_6011 = {
  id: '6011',
  type: 'links',
  title: 'Какая главная цель упаковки?',
  resultButton: false,
  tabs: [
    {
      title:
        '\u041a\u0430\u043a\u0430\u044f \u0433\u043b\u0430\u0432\u043d\u0430\u044f \u0446\u0435\u043b\u044c \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438?',
      cards: [
        {
          id: 'transportirovka',
          img: '/dev/nophoto.svg',
          title:
            '\u0422\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430',
        },
        {
          id: 'khranenie',
          img: '/dev/nophoto.svg',
          title: '\u0425\u0440\u0430\u043d\u0435\u043d\u0438\u0435',
        },
        {
          id: 'dostavka',
          img: '/dev/nophoto.svg',
          title: '\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430',
        },
        {
          id: 'roznichnaya-torgovlya',
          img: '/dev/nophoto.svg',
          title:
            '\u0420\u043e\u0437\u043d\u0438\u0447\u043d\u0430\u044f \u0442\u043e\u0440\u0433\u043e\u0432\u043b\u044f',
        },
      ],
    },
  ],
};

window.wizardStoreStep_6008 = {
  id: '6008',
  type: 'multiselect',
  title: 'Нужны ли дополнительные опции?',
  resultButton: false,
  tabs: [
    {
      title: '\u041e\u0431\u0449\u0430\u044f',
      cards: [
        {
          id: 'upakovka-s-pechatyu',
          img: '/upload/uf/4b8/Print_400.jpg',
          title:
            '\u0426\u0432\u0435\u0442\u043d\u0430\u044f \u043f\u0435\u0447\u0430\u0442\u044c',
        },
        {
          id: 'vstavki-reshetki-lozhementy-fiksatry',
          img: '/upload/uf/590/Logement_400.jpg',
          title:
            '\u0412\u0441\u0442\u0430\u0432\u043a\u0438, \u0440\u0435\u0448\u0435\u0442\u043a\u0438, \u043b\u043e\u0436\u0435\u043c\u0435\u043d\u0442\u044b, \u0444\u0438\u043a\u0441\u0430\u0442\u043e\u0440\u044b',
        },
        {
          id: 'kombinirovanye-materialy',
          img: '/upload/uf/713/Kombinir_400.jpg',
          title:
            '\u041a\u043e\u043c\u0431\u0438\u043d\u0438\u0440\u043e\u0432\u0430\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b',
        },
        {
          id: 'tisnenie',
          img: '/upload/uf/ac6/Tisnenie_400.jpg',
          title: '\u0422\u0438\u0441\u043d\u0435\u043d\u0438\u0435',
        },
        {
          id: 'vlagostoykoe-pokrytie',
          img: '/upload/uf/f57/Vlagostoykiy_400.jpg',
          title:
            '\u0412\u043b\u0430\u0433\u043e\u0441\u0442\u043e\u0439\u043a\u043e\u0435 \u043f\u043e\u043a\u0440\u044b\u0442\u0438\u0435',
        },
        {
          id: 'slozhnaya-vysechka',
          img: '/dev/nophoto.svg',
          title:
            '\u0421\u043b\u043e\u0436\u043d\u0430\u044f \u0432\u044b\u0441\u0435\u0447\u043a\u0430',
        },
        {
          id: 'usilennaya-konstruktsiya',
          img: '/upload/uf/893/Usilennaya_konstr_400.jpg',
          title:
            '\u0423\u0441\u0438\u043b\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u044f',
        },
        {
          id: 'okoshki',
          img: '/upload/uf/e6b/Okoshki_400.jpg',
          title: '\u041e\u043a\u043e\u0448\u043a\u0438',
        },
        {
          id: 'zamochki',
          img: '/upload/uf/bbb/Zamochki_400.jpg',
          title: '\u0417\u0430\u043c\u043e\u0447\u043a\u0438',
        },
        {
          id: 'ventilyatsionnye-otverstiya',
          img: '/upload/uf/b4c/Vent_otv_400.jpg',
          title:
            '\u0412\u0435\u043d\u0442\u0438\u043b\u044f\u0446\u0438\u043e\u043d\u043d\u044b\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u044f',
        },
        {
          id: 'ruchki',
          img: '/upload/uf/614/Ruchki_400.jpg',
          title: '\u0420\u0443\u0447\u043a\u0438',
        },
        {
          id: 'skobirovanie',
          img: '/upload/uf/374/Skobirovanie.jpg',
          title:
            '\u0421\u043a\u043e\u0431\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435',
        },
        {
          id: 'semnaya-kryshka',
          img: '/dev/nophoto.svg',
          title:
            '\u0421\u044a\u0435\u043c\u043d\u0430\u044f \u043a\u0440\u044b\u0448\u043a\u0430',
        },
        {
          id: 'kontrol-vskrytiya',
          img: '/upload/uf/523/Zashita_400.jpg',
          title:
            '\u041a\u043e\u043d\u0442\u0440\u043e\u043b\u044c \u0432\u0441\u043a\u0440\u044b\u0442\u0438\u044f',
        },
        {
          id: 'lyuversy',
          img: '/upload/uf/d87/Luvers_400.jpg',
          title: '\u041b\u044e\u0432\u0435\u0440\u0441\u044b',
        },
        {
          id: 'bag-in-box',
          img: '/upload/uf/6c7/s_paketom_400.jpg',
          title: 'Bag-in-Box',
        },
        {
          id: 'perforatsiya',
          img: '/upload/uf/515/Perf_400.jpg',
          title: '\u041f\u0435\u0440\u0444\u043e\u0440\u0430\u0446\u0438\u044f',
        },
        {
          id: 'nozhki',
          img: '/upload/uf/5c7/Nogki_400.jpg',
          title: '\u041d\u043e\u0436\u043a\u0438',
        },
        {
          id: 'vstroennyy-pallet',
          img: '/upload/uf/6f4/Vstr_pallet_400.jpg',
          title:
            '\u0412\u0441\u0442\u0440\u043e\u0435\u043d\u043d\u044b\u0439 \u043f\u0430\u043b\u043b\u0435\u0442',
        },
        {
          id: 'klapan-dozator',
          img: '/upload/uf/e5d/Klapan_400.jpg',
          title:
            '\u0414\u043e\u0437\u0438\u0440\u0443\u044e\u0449\u0438\u0439 \u043a\u043b\u0430\u043f\u0430\u043d',
        },
        {
          id: 'termostoykoe-pokrytie',
          img: '/upload/uf/1b6/Termo_400.jpg',
          title:
            '\u0422\u0435\u0440\u043c\u043e\u0441\u0442\u043e\u0439\u043a\u043e\u0435 \u043f\u043e\u043a\u0440\u044b\u0442\u0438\u0435',
        },
      ],
    },
  ],
};

window.wizardStoreStep_6009 = {
  id: '6009',
  type: 'form',
  title: 'Какие будут параметры упаковки?',
  resultButton: false,
  fields: {
    blocks: [
      {
        blockTitle:
          '\u0412\u043d\u0443\u0442\u0440\u0435\u043d\u043d\u0438\u0435 \u0440\u0430\u0437\u043c\u0435\u0440\u044b',
        blockFields: [
          {
            type: 'input-range',
            label: '\u0414\u043b\u0438\u043d\u0430',
            name: 'int-length',
            min: 0,
            max: 3000,
            step: 1,
            value: 600,
            unit: '\u043c\u043c',
          },
          {
            type: 'input-range',
            label: '\u0428\u0438\u0440\u0438\u043d\u0430',
            name: 'int-width',
            min: 0,
            max: 20160,
            step: 1,
            value: 4000,
            unit: '\u043c\u043c',
          },
          {
            type: 'input-range',
            label: '\u0412\u044b\u0441\u043e\u0442\u0430',
            name: 'int-height',
            min: 0,
            max: 5000,
            step: 1,
            value: 600,
            unit: '\u043c\u043c',
          },
        ],
      },
      {
        blockTitle:
          '\u0412\u043d\u0435\u0448\u043d\u0438\u0435 \u0440\u0430\u0437\u043c\u0435\u0440\u044b',
        blockFields: [
          {
            type: 'input-range',
            label: '\u0414\u043b\u0438\u043d\u0430',
            name: 'ext-length',
            min: 0,
            max: 3000,
            step: 1,
            value: 800,
            unit: '\u043c\u043c',
          },
          {
            type: 'input-range',
            label: '\u0428\u0438\u0440\u0438\u043d\u0430',
            name: 'ext-width',
            min: 0,
            max: 20160,
            step: 1,
            value: 5000,
            unit: '\u043c\u043c',
          },
          {
            type: 'input-range',
            label: '\u0412\u044b\u0441\u043e\u0442\u0430',
            name: 'ext-height',
            min: 0,
            max: 5000,
            step: 1,
            value: 800,
            unit: '\u043c\u043c',
          },
        ],
      },
      {
        blockTitle: '\u0422\u043e\u043b\u0449\u0438\u043d\u0430',
        blockFields: [
          {
            type: 'input-radio-img',
            label:
              '1-\u043d\u043e \u0441\u043b\u043e\u0439\u043d\u044b\u0439 \u0433\u043e\u0444\u0440\u043e\u043a\u0430\u0440\u0442\u043e\u043d',
            name: 'thickness',
            value: '1',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1520038410233-7141be7e6f97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\u0026ixlib=rb-1.2.1\u0026auto=format\u0026fit=crop\u0026w=753\u0026q=80',
            note:
              '\u003Cdiv class=\u0022row\u0022\u003E\u003Cdiv class=\u0022col-sm-6\u0022\u003E\u003Cimg src=\u0022./popup-img.png\u0022 alt=\u0022\u0022 class=\u0022img-responsive\u0022\u003E\u003C/div\u003E\u003Chr class=\u0022visible-xs-block\u0022\u003E\u003Cdiv class=\u0022col-sm-6\u0022\u003E\u003Ch3 style=\u0022margin-left\u0022 =\u003E 15px;\u0022\u003E\u0422\u0438\u0441\u043d\u0435\u043d\u0438\u0435 \u0444\u043e\u043b\u044c\u0433\u043e\u0439\u003C/h3\u003E\u003Cul\u003E\u003Cli\u003E\u0412 \u0440\u0430\u0437\u044b \u0443\u0432\u0435\u043b\u0438\u0447\u0438\u0432\u0430\u0435\u0442 \u043f\u0440\u0438\u0432\u043b\u0435\u043a\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438\u003C/li\u003E\u003Cli\u003E\u0418\u043c\u0435\u0435\u0442 \u0431\u043e\u043b\u044c\u0448\u0443\u044e \u0432\u0430\u0440\u0438\u0430\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c \u043f\u043e \u0446\u0432\u0435\u0442\u0430\u043c \u0438 \u044d\u0444\u0444\u0435\u043a\u0442\u0430\u043c\u003C/li\u003E\u003Cli\u003E\u042f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u043e\u0439 \u0438 \u0431\u044e\u0434\u0436\u0435\u0442\u043d\u043e\u0439 \u0430\u043b\u044c\u0442\u0435\u0440\u043d\u0430\u0442\u0438\u0432\u043e\u0439 \u0446\u0432\u0435\u0442\u043d\u043e\u0439 \u043f\u0435\u0447\u0430\u0442\u0438\u003C/li\u003E\u003Cli\u003E\u041c\u043e\u0436\u0435\u0442 \u043f\u0440\u0438\u043c\u0435\u043d\u044f\u0442\u044c\u0441\u044f \u043d\u0430 \u0431\u043e\u043b\u044c\u0448\u043e\u043c \u0444\u043e\u0440\u043c\u0430\u0442\u0435 \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438\u003C/li\u003E\u003Cli\u003E\u0425\u043e\u0440\u043e\u0448\u043e \u0441\u043e\u0447\u0435\u0442\u0430\u0435\u0442\u0441\u044f \u0441 \u0434\u0440\u0443\u0433\u0438\u043c\u0438 \u043e\u043f\u0446\u0438\u044f\u043c\u0438, \u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440, \u043f\u0435\u0447\u0430\u0442\u044c\u044e, \u0432\u044b\u0440\u0443\u0431\u043d\u044b\u043c\u0438 \u043e\u043a\u043e\u0448\u043a\u0430\u043c\u0438 \u0438 \u0442\u0434.\u003C/li\u003E\u003Cli\u003E\u0421\u043e\u0437\u0434\u0430\u0435\u0442 \u043f\u0440\u0438\u044f\u0442\u043d\u044b\u0435 \u0442\u0430\u043a\u0442\u0438\u043b\u044c\u043d\u044b\u0435 \u043e\u0449\u0443\u0449\u0435\u043d\u0438\u044f\u003C/li\u003E\u003Cli\u003E\u0421\u043e\u0447\u0435\u0442\u0430\u0435\u0442\u0441\u044f \u0441 \u043b\u044e\u0431\u044b\u043c \u0432\u0438\u0434\u043e\u043c \u0438 \u0446\u0432\u0435\u0442\u043e\u043c \u0433\u043e\u0444\u0440\u043e\u043a\u0430\u0440\u0442\u043e\u043d\u0430\u003C/li\u003E\u003C/ul\u003E\u003C/div\u003E\u003C/div\u003E',
          },
          {
            type: 'input-radio-img',
            label:
              '2-\u0445 \u0441\u043b\u043e\u0439\u043d\u044b\u0439 \u0433\u043e\u0444\u0440\u043e\u043a\u0430\u0440\u0442\u043e\u043d',
            name: 'thickness',
            value: '1',
            checked: true,
            img:
              'https://images.unsplash.com/photo-1556229040-2a7bc8a00a3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\u0026ixlib=rb-1.2.1\u0026auto=format\u0026fit=crop\u0026w=334\u0026q=80',
          },
          {
            type: 'input-radio-img',
            label:
              '3-\u0445 \u0441\u043b\u043e\u0439\u043d\u044b\u0439 \u0433\u043e\u0444\u0440\u043e\u043a\u0430\u0440\u0442\u043e\u043d',
            name: 'thickness',
            value: '1',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1598935888738-cd2622bcd437?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\u0026ixlib=rb-1.2.1\u0026auto=format\u0026fit=crop\u0026w=334\u0026q=80',
          },
        ],
      },
      {
        blockTitle: '\u0426\u0432\u0435\u0442',
        blockFields: [
          {
            type: 'input-radio-img',
            label: '\u0411\u0435\u043b\u044b\u0439',
            name: 'color',
            value: 'white',
            checked: true,
            img:
              'https://images.unsplash.com/photo-1520038410233-7141be7e6f97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\u0026ixlib=rb-1.2.1\u0026auto=format\u0026fit=crop\u0026w=753\u0026q=80',
          },
          {
            type: 'input-radio-img',
            label: '\u0411\u0443\u0440\u044b\u0439',
            name: 'brown',
            value: '1',
            checked: false,
            img:
              'https://images.unsplash.com/photo-1556229040-2a7bc8a00a3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\u0026ixlib=rb-1.2.1\u0026auto=format\u0026fit=crop\u0026w=334\u0026q=80',
          },
        ],
      },
      {
        blockTitle: '\u0422\u0438\u0440\u0430\u0436',
        blockFields: [
          {
            type: 'input-range',
            label:
              '\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e',
            name: 'quantity',
            min: 0,
            max: 1000,
            step: 1,
            value: 50,
            unit: '\u0448\u0442',
          },
        ],
      },
    ],
  },
};

window.wizardStoreStep_6010 = {
  id: '6010',
  type: 'poll',
  title: 'На чем важно сделать аккцент?',
  resultButton: true,
  tabs: [
    {
      title:
        '\u041f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u044c\u0441\u043a\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u044b',
      checkbox: [
        {
          name: 'speed',
          checked: 'false',
          label:
            '\u0421\u043a\u043e\u0440\u043e\u0441\u0442\u044c \u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435',
        },
        {
          name: 'wow',
          checked: 'false',
          label: 'Wow \u044d\u0444\u0444\u0435\u043a\u0442',
        },
        {
          name: 'simple',
          checked: 'false',
          label:
            '\u041f\u0440\u043e\u0441\u0442\u043e\u0442\u0430 \u0443\u0442\u0438\u043b\u0438\u0437\u0430\u0446\u0438\u0438',
        },
        { name: 'price', checked: 'false', label: '\u0426\u0435\u043d\u0430' },
      ],
    },
  ],
};
